#!/bin/bash
set -e

HE_DIR=$1
now="$(date +'%Y-%m-%dT%H:%M:%S.%3N')"
LAST_RUN_JSON=$HE_DIR/last-run-json
echo Last Run Directory: $LAST_RUN_JSON
sed -i -- "s/#startTime#/$now/g" $LAST_RUN_JSON/*.json
sed -i -- "s/#endTime#/$now/g" $LAST_RUN_JSON/*.json
